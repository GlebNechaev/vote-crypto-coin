import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlogPageComponent } from './pages/blog-page/blog-page.component';
import { PostsPageComponent } from './pages/posts-page/posts-page.component';
import { SinglPostComponent } from './pages/singl-post/singl-post.component';
const blogRoutes: Routes = [
  {
    path: '', component: BlogPageComponent, children: [
        { path: '', component: PostsPageComponent },
        { path: 'post/:id', component: SinglPostComponent },
    ]
    
  },
];

@NgModule({
    imports: [
        RouterModule.forChild(blogRoutes)
      ],
    exports: [RouterModule]
})
export class BlogRoutingModule { 

}
