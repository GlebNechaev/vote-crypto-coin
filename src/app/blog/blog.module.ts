import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { BlogRoutingModule } from "./blog-routing.module";
import { BlogPageComponent } from "./pages/blog-page/blog-page.component";
import { PostsPageComponent } from "./pages/posts-page/posts-page.component";
import { SinglPostComponent } from "./pages/singl-post/singl-post.component";

@NgModule({
    declarations: [	
        BlogPageComponent,
        SinglPostComponent,
        PostsPageComponent,
           ],
    imports: [
        CommonModule,
        BlogRoutingModule,
        SharedModule,
    ],
    exports: [
        RouterModule
    ]
})

export class BlogModule {

}