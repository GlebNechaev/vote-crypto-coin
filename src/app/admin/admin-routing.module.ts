import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminLayoutComponent } from './components/admin-layout/admin-layout.component';
import { AdminPageComponent } from './components/admin-page/admin-page.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PostCreateComponent } from './components/post-create/post-create.component';
import { PostEditComponent } from './components/post-edit/post-edit.component';

const adminRoutes: Routes = [
  {
    path: '', component: AdminLayoutComponent, children: [
        // { path: '', redirectTo: '/admin/login', pathMatch: 'full'},
        { path: '', component: AdminPageComponent },
        { path: 'dashboard', component: DashboardComponent },
        { path: 'create', component: PostCreateComponent },
        { path: 'post/:id/edit', component: PostEditComponent },
    ]
    
  },
];

@NgModule({
    imports: [
        RouterModule.forChild(adminRoutes)
      ],
    exports: [RouterModule]
})
export class AdminRoutingModule { 

}
