import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { AdminRoutingModule } from "./admin-routing.module";
import { AdminLayoutComponent } from "./components/admin-layout/admin-layout.component";
import { AdminPageComponent } from "./components/admin-page/admin-page.component";
import { AdminNavigationComponent } from "./components/tools/admin-navigation/admin-navigation.component";

@NgModule({
    declarations: [	
        AdminLayoutComponent,
        AdminNavigationComponent,
        AdminPageComponent
           ],
    imports: [
        SharedModule,
        AdminRoutingModule,
    ],
    exports: [
        RouterModule,
    ]
})

export class AdminModule {

}