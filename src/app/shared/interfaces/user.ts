export interface User {
    userName: string,
    password: string,
    email: string
}

export interface FbAuthResponce {
    idToken: string
    displayName: string
    email: string
    kind: string
    localId: string
    registered: boolean
}
