import { isPlatformServer } from '@angular/common';
import { Directive, Inject, PLATFORM_ID, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appAppShellNoRender]'
})
export class AppShellNoRenderDirective {

  constructor(
    @Inject(PLATFORM_ID) private platformId: any,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) { }

  ngOnInit(): void {
    if (!isPlatformServer(this.platformId)) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    }
  }

}
