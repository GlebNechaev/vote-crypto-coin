
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { NavigationComponent } from "./components/navigation/navigation.component";

import { AppShellNoRenderDirective } from "./directives/app-shell-no-render.directive";

@NgModule({
    declarations: [	
        AppShellNoRenderDirective,
        NavigationComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        HttpClientModule,
        RouterModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        HttpClientModule,
        RouterModule,
        
        AppShellNoRenderDirective, 
        NavigationComponent    
    ],
})

export class SharedModule {
}