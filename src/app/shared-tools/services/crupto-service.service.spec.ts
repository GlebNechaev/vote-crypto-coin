import { TestBed } from '@angular/core/testing';

import { CruptoServiceService } from './crupto-service.service';

describe('CruptoServiceService', () => {
  let service: CruptoServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CruptoServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
