import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, EMPTY, Observable} from 'rxjs';
import { catchError, retry, shareReplay} from 'rxjs/operators';
import { cryptoCurrencyInterface } from '../interface/cryptoCurrency';

@Injectable({
  providedIn: 'root'
})
export class CruptoServiceService {
  _currency!: cryptoCurrencyInterface[];
  
  private cryptosMassive = new BehaviorSubject<cryptoCurrencyInterface[]>([]);
  public cryptosMassive$ = this.cryptosMassive.asObservable();

  constructor(
    private http: HttpClient
  ) { }

  getCryptosCurrency(): Observable<any> {
    return this.http.get<any>('https://api.coincap.io/v2/assets')
    .pipe(
      retry(3),
      catchError(() => {
        return EMPTY;
      }),
      shareReplay()
    );
  }

  getCryptoCurrency(id: string): Observable<any> {
    return this.http.get<any>('https://api.coincap.io/v2/assets/' + id)
    .pipe(
      retry(3),
      catchError(() => {
        return EMPTY;
      }),
      shareReplay()
    );
  }

  shareCryptos(cryptosMassive: cryptoCurrencyInterface[]) {
    this.cryptosMassive.next(cryptosMassive);
  }
}

