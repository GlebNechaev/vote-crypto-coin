import { Component, OnInit } from '@angular/core';
import { cryptoCurrencyInterface } from 'src/app/shared-tools/interface/cryptoCurrency';
import { CruptoServiceService } from 'src/app/shared-tools/services/crupto-service.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  subscriptionCryptos: any;
  cryptoFilter!: cryptoCurrencyInterface[];
  focusSearch: boolean = false;
  searchText: string = '';


  constructor(
    private сruptoServiceService: CruptoServiceService,
  ) { }

  ngOnInit() {
   this.subscriptionCryptos = this.сruptoServiceService.cryptosMassive$
    .subscribe((cryptos) => {
      this.cryptoFilter = cryptos
    });
  }

  ngOnDestroy(): void {
    this.subscriptionCryptos.unsubscribe();
  }

  blur(){
    this.focusSearch = false;
  }

  focus(){
    this.focusSearch = true;
  }
}
