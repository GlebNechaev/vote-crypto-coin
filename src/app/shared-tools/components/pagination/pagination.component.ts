import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { cryptoCurrencyInterface } from '../../interface/cryptoCurrency';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  cryptosSlice: number = 0
  pages: number[] = [];
  activePage: number = 1
  notesOnPage = 20;
  cryptosFromPaginator: cryptoCurrencyInterface[] = [];
  
  notesStart: number = 0
  notesEnd: number = 0
  
  @Input() set cryptoFromDatabaseLength(value: number) {
    if (!value) {
      return;
    }
    this.cryptosSlice = value;
    this.getPagesPaginator();
  };
  @Output() pagePaginator: EventEmitter<number[]> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  setNotesPaginator(pageNum: number){
    this.activePage = pageNum
    this.notesStart = (pageNum - 1) * this.notesOnPage;
    this.notesEnd = this.notesStart + this.notesOnPage;
    let notes: number[] = []
    notes.push(this.notesStart, this.notesEnd);
    this.pagePaginator.emit(notes)
  }

  getPagesPaginator(){
    this.pages = []
    let cryptoPages = Math.ceil(this.cryptosSlice / this.notesOnPage) 
    for (let i = 1; i <= cryptoPages; i++) {
      this.pages.push(i);
    }
  }

  setLastPage() {
    this.activePage = this.pages.length;
    this.setNotesPaginator(this.activePage);
  }

  setFirstPage() {
    this.activePage = 1;
    this.setNotesPaginator(this.activePage);
  }
}
