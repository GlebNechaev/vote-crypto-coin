import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'currencyPipe'
})
export class CurrencyPipe implements PipeTransform {

  transform(currency: number): number {
    if (currency > 0.97) {
      const x = +currency
      currency = +x.toFixed(2)

    } else {
      const x = +currency
      currency = +x.toFixed(8)
    }
    return currency;
  }

}
