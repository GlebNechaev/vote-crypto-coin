
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { CurrencyPipe } from "./pipes/currency/currency.pipe";
import { FilterPipe } from "./pipes/filter-pipe/filter-pipe.pipe";

import { MainRoutingModule } from "../main/main-routing.module";
import { CurrencyComponent } from "./components/cryptos-menu/currency/currency.component";
import { MenuToolsComponent } from "./components/cryptos-menu/menu-tools.component";
import { SearchComponent } from "./components/cryptos-menu/search/search.component";
import { PaginationComponent } from "./components/pagination/pagination.component";

@NgModule({
    declarations: [	
    PaginationComponent,
    MenuToolsComponent,
    SearchComponent,
    CurrencyComponent,	
    CurrencyPipe,
    FilterPipe,
       ],
    imports: [
    MainRoutingModule,
    CommonModule,
    FormsModule,
    ],
    exports: [
    PaginationComponent,
    MenuToolsComponent,
    SearchComponent,
    CurrencyComponent,	
    CurrencyPipe,
    FilterPipe,
    FormsModule,
    ],
})

export class SharedToolsModule {
}