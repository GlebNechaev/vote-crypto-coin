import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CryptoPageComponent } from './pages/crypto-page/crypto-page.component';
import { CryptosPageComponent } from './pages/cryptos-page/cryptos-page.component';
import { MainPageComponent } from './pages/main-page/main-page.component';

const mainRoutes: Routes = [
  {
    path: '', component: MainPageComponent, children: [
      { path: '', redirectTo: '/', pathMatch: 'full'},
      { path: '', component: CryptosPageComponent },
      { path: 'cryptos/:id', component: CryptoPageComponent },
    ]
  },
];

@NgModule({
    imports: [
        RouterModule.forChild(mainRoutes)
      ],
    exports: [RouterModule]
})
export class MainRoutingModule { 

}
