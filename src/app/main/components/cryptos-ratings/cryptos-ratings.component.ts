import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import * as _ from 'lodash'; 
import { cryptoCurrencyInterface } from 'src/app/shared-tools/interface/cryptoCurrency';
import { CruptoServiceService } from 'src/app/shared-tools/services/crupto-service.service';

@Component({
  selector: 'app-cryptos-ratings',
  templateUrl: './cryptos-ratings.component.html',
  styleUrls: ['./cryptos-ratings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class CryptosRatingsComponent implements OnInit {

  cryptos: cryptoCurrencyInterface[] = [];
  biggestValues: cryptoCurrencyInterface[] = [];
  lowestValues: cryptoCurrencyInterface[] = [];



  public biggestLosers$ = this.сruptoServiceService.cryptosMassive$
    .pipe(
      map(crypts => {
        return [...crypts].sort((a, b) => a.changePercent24Hr - b.changePercent24Hr).slice(0, 3);
      }),
    )

  public biggestGainers$ = this.сruptoServiceService.cryptosMassive$
    .pipe(
      map(crypts => {
        return [...crypts].sort((a, b) => b.changePercent24Hr - a.changePercent24Hr).slice(0, 3);
      })
    )

  constructor(
    private сruptoServiceService: CruptoServiceService,
  ) { }

  ngOnInit() {
  }  
}

