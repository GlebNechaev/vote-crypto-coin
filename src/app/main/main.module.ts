
import { NgModule } from "@angular/core";

import { CruptoServiceService } from "../shared-tools/services/crupto-service.service";
import { SharedToolsModule } from "../shared-tools/shared-tools.module";
import { SharedModule } from "../shared/shared.module";
import { CryptosRatingsComponent } from "./components/cryptos-ratings/cryptos-ratings.component";
import { VoteBanerComponent } from "./components/vote-baner/vote-baner.component";
import { MainRoutingModule } from "./main-routing.module";
import { CryptoPageComponent } from "./pages/crypto-page/crypto-page.component";
import { CryptosPageComponent } from "./pages/cryptos-page/cryptos-page.component";
import { MainPageComponent } from "./pages/main-page/main-page.component";


@NgModule({
    declarations: [		
    MainPageComponent,
    CryptosPageComponent,
    CryptosRatingsComponent,
    CryptoPageComponent,
    VoteBanerComponent,	
       ],
    imports: [
    MainRoutingModule,
    SharedToolsModule,
    SharedModule
    ],
    providers: [CruptoServiceService],
})

export class MainModule {
}