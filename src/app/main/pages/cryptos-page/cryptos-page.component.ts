import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { cryptoCurrencyInterface } from 'src/app/shared-tools/interface/cryptoCurrency';
import { CruptoServiceService } from 'src/app/shared-tools/services/crupto-service.service';

@Component({
  selector: 'app-cryptos-page',
  templateUrl: './cryptos-page.component.html',
  styleUrls: ['./cryptos-page.component.scss']
})

export class CryptosPageComponent implements OnInit {
 cryptoFromDatabase!: cryptoCurrencyInterface[];
 cryptoFromPaginator!: cryptoCurrencyInterface[];
 cryptoFromDatabaseLenght: number = 0;



 private subscriptionCryptos?: Subscription;
 private timer$ = timer(0, 120000);

  constructor(
    private сruptoServiceService: CruptoServiceService,
    @Inject(PLATFORM_ID) private platformId: any
  ) { }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
      this.subscriptionCryptos = this.timer$
      .pipe(
        switchMap((val) => {
          return this.сruptoServiceService.getCryptosCurrency()
        })
      )
      .subscribe((cryptos) => {
        console.log(cryptos)
        this.сruptoServiceService.shareCryptos(cryptos.data)
        this.cryptoFromDatabase = cryptos.data
        this.cryptoFromDatabaseLenght = this.cryptoFromDatabase.length
        this.getPagePaginator([0,20])
      });
    }
    
  }
  
  ngOnDestroy(): void {
    this.subscriptionCryptos?.unsubscribe();
  }
  
  getPagePaginator(event: number[]){
    let notesStart = event[0]
    let notesEnd = event[1]
    let cruptoMassiv = this.cryptoFromDatabase
    this.cryptoFromPaginator = cruptoMassiv.slice(notesStart, notesEnd)
  }
  
}

