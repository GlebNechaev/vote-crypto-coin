import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { cryptoCurrencyInterface } from 'src/app/shared-tools/interface/cryptoCurrency';
import { CruptoServiceService } from 'src/app/shared-tools/services/crupto-service.service';

@Component({
  selector: 'app-crypto-page',
  templateUrl: './crypto-page.component.html',
  styleUrls: ['./crypto-page.component.scss']
})
export class CryptoPageComponent implements OnInit {
  _currency!: cryptoCurrencyInterface;
  subscriptionCryptos: any 

  constructor(
    private route: ActivatedRoute,
    private cruptoServiceService: CruptoServiceService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params : Params) => {
    const id = params.id
    console.log(params)
    this.getCrypto(id)
    })
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscriptionCryptos.unsubscribe();
  }
  
  getCrypto(id: string){
    this.subscriptionCryptos = this.cruptoServiceService.getCryptoCurrency(id)
    .subscribe((data) => {
      this._currency = data.data;
    })
  }


}
