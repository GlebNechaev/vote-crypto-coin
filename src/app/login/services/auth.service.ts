import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, pipe } from 'rxjs';
import { tap } from 'rxjs/operators';
import { FbAuthResponce, User } from 'src/app/shared/interfaces/user';
import { environment } from 'src/environments/environment';

@Injectable()

export class AuthService {

constructor(private http: HttpClient) { }

    get token(): string {
        return ''
    }

    private setToken(responce: any) {
        console.log(responce)
    }

    login(user: User): Observable<FbAuthResponce> {
        return this.http.post(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.apiKey}`, user)
        .pipe(
            tap(this.setToken)
        )
    }
    
    logOut() {

    }

    register() {

    }

    isAuthentification() {
        return !!this.token
    }

}
