import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { LoginRoutingModule } from "./login-routing.module";
import { LoginPageComponent } from "./pages/login-page/login-page.component";
import { AuthService } from "./services/auth.service";

@NgModule({
    declarations: [	
        LoginPageComponent
           ],
    imports: [
        LoginRoutingModule,
        ReactiveFormsModule,
        SharedModule,
    ],
    exports: [
        RouterModule,
    ],
    providers: [
        AuthService
    ]
})

export class LoginModule {

}