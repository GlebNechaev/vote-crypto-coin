import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';4

import { User } from '../../../shared/interfaces/user';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit {
  registerForm: FormGroup = new FormGroup({});

  authentificator: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private auth: AuthService) {}

  ngOnInit() {
    if (this.router.url == '/login') {
      this.authentificator = true
    } else {
      this.authentificator = false
    }
    this.createForm();
  }

  createForm() {
    this.registerForm = this.formBuilder.group({
      // userName: [
      //   '',
      //   [
      //     Validators.required,
      //     Validators.minLength(3),
      //     Validators.maxLength(30),
      //   ],
      // ],
      password: ['', [Validators.required, Validators.minLength(6)]],
      email: ['', [Validators.email, Validators.required]],
      // confirmPassword: ['', [Validators.minLength(6), Validators.required]],
    },
    { 
      // validator: ConfirmedValidator('password', 'confirmPassword')
    });
  }

  get checkForm(){
    return this.registerForm.controls;
  }
  
  authChanged() {
    this.authentificator ? this.router.navigate(['/login/register']) : this.router.navigate(['/login'])
 
  }

  login() {
    if (this.registerForm.invalid) {
      return;
    }
    const user: User = {
      userName: this.registerForm.value.userName,
      password: this.registerForm.value.password,
      email: this.registerForm.value.email
    }
    this.auth.login(user).subscribe(() => {
      this.registerForm.reset
      this.router.navigate(['/admin'])
    })
  }

  register() {
    const user: User = {
      userName: this.registerForm.value.userName,
      password: this.registerForm.value.password,
      email: this.registerForm.value.email
    }
  }
}
